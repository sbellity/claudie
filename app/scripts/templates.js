this["Hull"] = this["Hull"] || {};
this["Hull"]["templates"] = this["Hull"]["templates"] || {};

this["Hull"]["templates"]["activity/activity"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n    <li class=\"media\">\n      ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.actor),stack1 == null || stack1 === false ? stack1 : stack1.picture), {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n      <div class=\"media-body\">\n        <h6 class=\"media-heading\">\n          "
    + escapeExpression(((stack1 = ((stack1 = depth0.actor),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\n          <p class=\"muted\"><small>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fromNow || depth0.fromNow),stack1 ? stack1.call(depth0, depth0.published, options) : helperMissing.call(depth0, "fromNow", depth0.published, options)))
    + "</small></p>\n        </h6>\n        ";
  options = {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data};
  stack2 = ((stack1 = helpers.ifEqual || depth0.ifEqual),stack1 ? stack1.call(depth0, ((stack1 = depth0.target),stack1 == null || stack1 === false ? stack1 : stack1.type), "image", options) : helperMissing.call(depth0, "ifEqual", ((stack1 = depth0.target),stack1 == null || stack1 === false ? stack1 : stack1.type), "image", options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n        <h6>"
    + escapeExpression(((stack1 = ((stack1 = depth0.target),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</h6>\n        ";
  if (stack2 = helpers.content) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.content; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\n      </div>\n      </div>\n    </li>\n  ";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n        <div class=\"pull-left\">\n          <img class=\"media-object img-circle\" src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.actor),stack1 == null || stack1 === false ? stack1 : stack1.picture)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" width=\"36px\" />\n        </div>\n      ";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n          <a href=\"/#/selection/"
    + escapeExpression(((stack1 = ((stack1 = depth0.target),stack1 == null || stack1 === false ? stack1 : stack1.id)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\"><img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.imageUrl || depth0.imageUrl),stack1 ? stack1.call(depth0, ((stack1 = depth0.target),stack1 == null || stack1 === false ? stack1 : stack1.id), "small", options) : helperMissing.call(depth0, "imageUrl", ((stack1 = depth0.target),stack1 == null || stack1 === false ? stack1 : stack1.id), "small", options)))
    + "\"/></a>\n        ";
  return buffer;
  }

function program6(depth0,data) {
  
  
  return "\n    <li>\n      <div class=\"alert\">No activity</div>\n    </li>\n  ";
  }

function program8(depth0,data) {
  
  
  return "\n  <ul class=\"pager\">\n    <li class=\"previous\">\n      <a href=\"#\" data-hull-action=\"previousPage\">&larr; Previous</a>\n    </li>\n    <li class=\"next\">\n      <a href=\"#\" d data-hull-action=\"nextPage\">Next &rarr;</a>\n    </li>\n  </ul>\n";
  }

function program10(depth0,data) {
  
  
  return "\n  <button data-hull-action=\"fetchMore\" class=\"btn btn-block\">Fetch more...</button>\n";
  }

  buffer += "<ul class=\"hull-activity media-list\">\n  ";
  stack1 = helpers.each.call(depth0, depth0.activities, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n  ";
  stack1 = helpers.unless.call(depth0, depth0.activities, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</ul>\n\n\n";
  stack1 = helpers['if'].call(depth0, depth0.isPaged, {hash:{},inverse:self.program(10, program10, data),fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  };

this["Hull"]["templates"]["app/index"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, self=this;

function program1(depth0,data) {
  
  
  return "\n    <p><a href=\"#submit\" class=\"btn btn-primary btn-large\">Make your selection!</a></p>\n  ";
  }

function program3(depth0,data) {
  
  
  return "\n  <div class=\"row\">\n    <div class=\"span9\" data-hull-component=\"list\"></div>\n    <div class=\"span3\" data-hull-component=\"activity@hull\" data-hull-object_type=\"Comment\"></div>\n  </div>\n";
  }

function program5(depth0,data) {
  
  
  return "\n  <div class=\"alert alert-warning\">Sign in to play</div>\n";
  }

  buffer += "<div class=\"hero-unit\">\n  <div class=\"pull-right\" data-hull-component=\"identity@hull\"></div>\n\n  <h1>\n    Claudie Pierlot\n  </h1>\n  <p>Create your own compositions !</p>\n\n  ";
  stack1 = helpers['if'].call(depth0, depth0.loggedIn, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</div>\n\n";
  stack1 = helpers['if'].call(depth0, depth0.loggedIn, {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n";
  return buffer;
  };

this["Hull"]["templates"]["app/single"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div data-hull-component=\"single\" data-hull-id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"></div>\n";
  return buffer;
  };

this["Hull"]["templates"]["app/submit"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"row\">\n  <div class=\"span6\" data-hull-component=\"products\"></div>\n  <div class=\"span6\" data-hull-component=\"selection\"></div>\n</div>\n";
  };

this["Hull"]["templates"]["list/main"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n    <li class=\"span2\">\n      <div class=\"thumbnail\">\n        <a href=\"#/selection/";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"><img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.imageUrl || depth0.imageUrl),stack1 ? stack1.call(depth0, depth0.id, "medium", options) : helperMissing.call(depth0, "imageUrl", depth0.id, "medium", options)))
    + "\" /></a>\n        <h5>";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</h5>\n        <p class=\"muted\">\n          <a class='btn-link pull-right' data-hull-action='showDetails' data-hull-selection-id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">details</a>\n          <small>By "
    + escapeExpression(((stack1 = ((stack1 = depth0.actor),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</small>\n        </p>\n        \n      </div>\n    </li>\n  ";
  return buffer;
  }

  buffer += "<ul class=\"thumbnails row\">\n  ";
  stack1 = helpers.each.call(depth0, depth0.images, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</ul>\n<div data-hull-component=\"selection-details\"></div>";
  return buffer;
  };

this["Hull"]["templates"]["products/main"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n    <li>\n      <a href=\"\" data-hull-action=\"toggle\" data-hull-product=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"list-group-item\">\n        <img src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.extra),stack1 == null || stack1 === false ? stack1 : stack1.image)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" width=\"50px\"/>\n        ";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\n      </a>\n    </li>\n  ";
  return buffer;
  }

  buffer += "<h1>Products</h1>\n<ul class=\"nav nav-tabs nav-stacked\">\n  ";
  stack1 = helpers.each.call(depth0, depth0.products, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</ul>\n";
  return buffer;
  };

this["Hull"]["templates"]["selection-details/main"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n        <li class=\"span4\">\n          <div class=\"thumbnail\">\n            <a href=\"";
  if (stack1 = helpers.uid) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.uid; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" target=\"store\">\n              <img src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.extra),stack1 == null || stack1 === false ? stack1 : stack1.image)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" />\n              <h5>";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</h5>\n            </a>\n          </div>\n        </li>\n        ";
  return buffer;
  }

  buffer += "<div class=\"modal hide fade\">\n  <div class=\"modal-header\">\n    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>\n    <h3>"
    + escapeExpression(((stack1 = ((stack1 = depth0.selection),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</h3>\n    <p class=\"muted\">by "
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.selection),stack1 == null || stack1 === false ? stack1 : stack1.actor)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\n  </div>\n  <div class=\"modal-body\">\n    <p>"
    + escapeExpression(((stack1 = ((stack1 = depth0.selection),stack1 == null || stack1 === false ? stack1 : stack1.description)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</p>\n    <div class=\"row-fluid\">\n      <ul class=\"thumbnails\">\n        ";
  stack2 = helpers.each.call(depth0, ((stack1 = ((stack1 = depth0.selection),stack1 == null || stack1 === false ? stack1 : stack1.extra)),stack1 == null || stack1 === false ? stack1 : stack1.selection), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n      </ul>\n    </div>\n  </div>\n</div>";
  return buffer;
  };

this["Hull"]["templates"]["selection/main"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n<div class=\"row-fluid well text-center\">\n  ";
  stack1 = helpers.each.call(depth0, depth0.selection, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</div>\n\n";
  stack1 = helpers['if'].call(depth0, depth0.complete, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n    <div class=\"span4\">\n      <img src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.extra),stack1 == null || stack1 === false ? stack1 : stack1.image)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" width=\"120px\"/>\n      <h5>";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</h5>\n    </div>\n  ";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n<form>\n  <fieldset>\n    <legend>Save your Selection</legend>\n    <label>Give it a name</label>\n    <input type=\"text\" class=\"input-block-level js-claudie-selection-name\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.me),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " selection\">\n    <label>and a description</label>\n    <textarea type=\"text\" class=\"input-block-level js-claudie-selection-description\"></textarea>\n    <a href=\"#\" data-hull-action=\"submit\" class=\"btn btn-primary btn-lg btn-block\">Submit</a>\n  </fieldset>\n</form>\n";
  return buffer;
  }

function program6(depth0,data) {
  
  
  return "\n<div class=\"well\">\n  Add some products to your selection\n</div>\n";
  }

  buffer += "<h1>Selection</h1>\n\n";
  stack1 = helpers['if'].call(depth0, depth0.selection, {hash:{},inverse:self.program(6, program6, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  };

this["Hull"]["templates"]["single/main"] = function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n<div class=\"row\">\n  <div class=\"span12\">\n    <h2>\n      <div class=\"pull-right\" data-hull-component=\"like_button@hull\" data-hull-id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"></div>\n      ";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n    </h2>\n    <p>\n      By "
    + escapeExpression(((stack1 = ((stack1 = depth0.actor),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " – ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.fromNow || depth0.fromNow),stack1 ? stack1.call(depth0, depth0.created_at, options) : helperMissing.call(depth0, "fromNow", depth0.created_at, options)))
    + "\n    </p>\n  </div>\n  <div class=\"span6\">\n    <img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.imageUrl || depth0.imageUrl),stack1 ? stack1.call(depth0, depth0.id, "medium", options) : helperMissing.call(depth0, "imageUrl", depth0.id, "medium", options)))
    + "\"  />\n    <p>";
  if (stack2 = helpers.description) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.description; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</p>\n    <h5>Selection</h5>\n    <ul class=\"nav nav-tabs nav-stacked\">\n      ";
  stack2 = helpers.each.call(depth0, ((stack1 = depth0.extra),stack1 == null || stack1 === false ? stack1 : stack1.selection), {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n    </ul>\n  </div>\n  <div class=\"span6\">\n    <h3>Comments</h3>\n    <div data-hull-component=\"comments@hull\" data-hull-id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></div>\n  </div>\n</div>\n";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n        <li>\n          <a href=\"";
  if (stack1 = helpers.uid) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.uid; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" target=\"store\" class=\"list-group-item\">\n            <img src=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.extra),stack1 == null || stack1 === false ? stack1 : stack1.image)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" width=\"36\" />\n            ";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\n          </a>\n        </li>\n      ";
  return buffer;
  }

  stack1 = helpers['with'].call(depth0, depth0.image, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  };