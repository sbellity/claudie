Hull.widget('app', {
  templates: [
    'index',
    'submit',
    'single'
  ],

  refreshEvents: ['model.hull.me.change'],

  initialize: function() {
    var Backbone = Hull.require('backbone');

    var self = this;
    var Router = Backbone.Router.extend({
      routes: {
        'submit': 'showSubmit',
        'selection/:id': 'showSelection',
        '*path': 'showIndex'
      },

      showIndex: function() { self.render('index'); },
      showSubmit: function() { self.render('submit'); },
      showSelection: function(id) { self.render('single', { id: id }); }
    });

    this.router = new Router();

    this.sandbox.on('claudie.navigate', function(route) {
      this.router.navigate(route, { trigger: true });
    }, this);
  }
});



//--------


Hull.component('list', {
  templates: ['main'],

  datasources: {
    images: function() {
      var _ =  this.sandbox.util._;
      return this.api('app/activity', {
        per_page: 64,
        where: { obj_type: 'Image', verb: 'create' },
        order_by: 'created_at DESC'
      }).pipe(function(activities) {
        return _.chain(activities).pluck('object').filter(function(a) { return a; }).value();
      });
    }
  },
  
  actions: {
    showDetails: function(event, action) {
      console.warn('action', action);
      this.sandbox.emit('claudie.selection.details', action.data.selectionId);
    }
  },

  beforeRender: function(data) {
    console.warn("Rendering list : ", data.images);
  }
});




//--------


Hull.component('products', {
  templates: [
    'main'
  ],

  datasources: {
    products: { path: 'app/entities', where: { type: 'product' } }
  },

  initialize: function() {
    this.selection = {};

    this.sandbox.on('claudie.product.toggle', this.toggleProduct, this);
  },

  getSelectedCount: function() {
    return this.sandbox.util._.filter(this.selection, function(v) {
      return v;
    }).length;
  },

  getSelectedProducts: function() {
    return this.sandbox.util._.filter(this.data.products.toJSON(), function(p) {
      return this.selection[p.id];
    }, this);
  },

  productIsSelected: function(product) {
    return !!this.selection[product];
  },

  toggleProduct: function(el, product) {
    if (this.getSelectedCount() < 3 || this.productIsSelected(product)) {
      this.selection[product] = !this.selection[product];
      $(el).toggleClass('selected', this.selection[product]);
      this.sandbox.emit('claudie.selection.change', this.getSelectedProducts());
    } else {
      alert('Remove a product if you want to add this one');
    }
  },

  actions: {
    toggle: function(event, action) {
      this.toggleProduct(event.currentTarget, action.data.product);
    }
  }
});




//--------


Hull.component('selection-details', {
  templates: ['main'],
  datasources: {
    selection: function() {
      if (this.id) {
        return this.api(this.id);
      }
    }
  },
  initialize: function() {
    this.sandbox.on('claudie.selection.details', function(selectionId) {
      this.id = selectionId;
      this.render();
      this.show();
    }, this);
  },
  afterRender: function(data) {
    if (data.selection) {
      this.modalEl = this.$find('.modal');
      this.modalEl.modal();
    } else {
      this.modalEl = false;
    }
  },
  show: function() {
    this.modalEl && this.modalEl.modal('show');
  }
})



//--------


Hull.component('selection', {
  templates: [
    'main'
  ],

  initialize: function() {
    this.selection = [];
    this.sandbox.on('claudie.selection.change', this.onSelectionChange, this);
  },

  beforeRender: function(data) {
    data.selection = this.selection;
    data.complete = (this.selection.length === 3);
  },

  afterRender: function() {
    this.$('.js-claudie-selection-name').focus();
  },

  actions: {

    submit: function(event, action) {
      event.preventDefault();
      if (this.selection.length === 3) {
        var self = this;
        var attrs = {
          name: this.$('.js-claudie-selection-name').val(),
          description: this.$('.js-claudie-selection-description').val(),
          source_url: 'http://placehold.it/200x200/', // Generate image here ?
          extra: { selection: this.selection }
        }
        this.api('me/images', 'post', attrs).done(function(res) {
          self.sandbox.emit('claudie.navigate', '/selection/' + res.id);
        });
      } else {
        alert('You must choose three products to participate');
      }
    }
  },

  onSelectionChange: function(selection) {
    this.selection = selection;
    this.render();
  }
});




//--------


Hull.component('single', {
  templates: [ 'main' ],

  datasources: {
    image: ':id'
  },

  beforeRender: function(data) {
    if (!data.image || data.image.type !== 'image') {
      this.sandbox.emit('claudie.navigate', '');
      return false;
    }
  }
});
