Hull.component('products', {
  templates: [
    'main'
  ],

  datasources: {
    products: { path: 'app/entities', where: { type: 'product' } }
  },

  initialize: function() {
    this.selection = {};

    this.sandbox.on('claudie.product.toggle', this.toggleProduct, this);
  },

  getSelectedCount: function() {
    return this.sandbox.util._.filter(this.selection, function(v) {
      return v;
    }).length;
  },

  getSelectedProducts: function() {
    return this.sandbox.util._.filter(this.data.products.toJSON(), function(p) {
      return this.selection[p.id];
    }, this);
  },

  productIsSelected: function(product) {
    return !!this.selection[product];
  },

  toggleProduct: function(el, product) {
    if (this.getSelectedCount() < 3 || this.productIsSelected(product)) {
      this.selection[product] = !this.selection[product];
      $(el).toggleClass('selected', this.selection[product]);
      this.sandbox.emit('claudie.selection.change', this.getSelectedProducts());
    } else {
      alert('Remove a product if you want to add this one');
    }
  },

  actions: {
    toggle: function(event, action) {
      this.toggleProduct(event.currentTarget, action.data.product);
    }
  }
});
