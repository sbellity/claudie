Hull.component('single', {
  templates: [ 'main' ],

  datasources: {
    image: ':id'
  },

  beforeRender: function(data) {
    if (!data.image || data.image.type !== 'image') {
      this.sandbox.emit('claudie.navigate', '');
      return false;
    }
  }
});
