Hull.widget('app', {
  templates: [
    'index',
    'submit',
    'single'
  ],

  refreshEvents: ['model.hull.me.change'],

  initialize: function() {
    var Backbone = Hull.require('backbone');

    var self = this;
    var Router = Backbone.Router.extend({
      routes: {
        'submit': 'showSubmit',
        'selection/:id': 'showSelection',
        '*path': 'showIndex'
      },

      showIndex: function() { self.render('index'); },
      showSubmit: function() { self.render('submit'); },
      showSelection: function(id) { self.render('single', { id: id }); }
    });

    this.router = new Router();

    this.sandbox.on('claudie.navigate', function(route) {
      this.router.navigate(route, { trigger: true });
    }, this);
  }
});