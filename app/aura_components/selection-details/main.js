Hull.component('selection-details', {
  templates: ['main'],
  datasources: {
    selection: function() {
      if (this.id) {
        return this.api(this.id);
      }
    }
  },
  initialize: function() {
    this.sandbox.on('claudie.selection.details', function(selectionId) {
      this.id = selectionId;
      this.render();
      this.show();
    }, this);
  },
  afterRender: function(data) {
    if (data.selection) {
      this.modalEl = this.$find('.modal');
      this.modalEl.modal();
    } else {
      this.modalEl = false;
    }
  },
  show: function() {
    this.modalEl && this.modalEl.modal('show');
  }
})