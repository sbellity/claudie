Hull.component('selection', {
  templates: [
    'main'
  ],

  initialize: function() {
    this.selection = [];
    this.sandbox.on('claudie.selection.change', this.onSelectionChange, this);
  },

  beforeRender: function(data) {
    data.selection = this.selection;
    data.complete = (this.selection.length === 3);
  },

  afterRender: function() {
    this.$('.js-claudie-selection-name').focus();
  },

  actions: {

    submit: function(event, action) {
      event.preventDefault();
      if (this.selection.length === 3) {
        var self = this;
        var attrs = {
          name: this.$('.js-claudie-selection-name').val(),
          description: this.$('.js-claudie-selection-description').val(),
          source_url: 'http://placehold.it/200x200/', // Generate image here ?
          extra: { selection: this.selection }
        }
        this.api('me/images', 'post', attrs).done(function(res) {
          self.sandbox.emit('claudie.navigate', '/selection/' + res.id);
        });
      } else {
        alert('You must choose three products to participate');
      }
    }
  },

  onSelectionChange: function(selection) {
    this.selection = selection;
    this.render();
  }
});
