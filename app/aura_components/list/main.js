Hull.component('list', {
  templates: ['main'],

  datasources: {
    images: function() {
      var _ =  this.sandbox.util._;
      return this.api('app/activity', {
        per_page: 64,
        where: { obj_type: 'Image', verb: 'create' },
        order_by: 'created_at DESC'
      }).pipe(function(activities) {
        return _.chain(activities).pluck('object').filter(function(a) { return a; }).value();
      });
    }
  },
  
  actions: {
    showDetails: function(event, action) {
      console.warn('action', action);
      this.sandbox.emit('claudie.selection.details', action.data.selectionId);
    }
  },

  beforeRender: function(data) {
    console.warn("Rendering list : ", data.images);
  }
});
